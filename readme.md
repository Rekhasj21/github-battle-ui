
<h1 align="center">Github Battle UI</h1>

## Table of Contents

- [Overview](#overview)
- [Project Link](#project-link)
- [Built With](#built-with)
- [How to use](#how-to-use)
- [Contact](#contact)

## Overview

**Light Mode**

<a href="" rel="light mode"><img src="public/light-mode.png" alt="" width="200px"/></a>

**Dark Mode**

<a href="" rel="dark mode"><img src="public/dark-mode.png" alt="" width="200px"/></a>


## Project Link

<a href="https://rekha-sj-github-battle-ui.netlify.app/" >Click To Check Project Link</a>

### Built With

- HTML
- Bootstrap
- React JS
- Redux Toolkit

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone git@gitlab.com:Rekhasj21/github-battle-ui.git

# Install dependencies
$ npm install

# Run the app
$ npm start
```

## Contact

- Website (<https://rekha-sj-github-battle-ui.netlify.app/>)
- GitHub (<https://gitlab.com/Rekhasj21/github-battle-ui>)