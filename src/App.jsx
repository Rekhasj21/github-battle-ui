import React from 'react'
import { useEffect } from 'react'
import { getBattleData } from './Features/Battle/BattleSlice'
import { useDispatch, useSelector } from 'react-redux'
import BattleContainer from './components/BattleContainer'
import Navbar from './components/Navbar'
import LanguagesHeading from './components/LanguagesHeading'

function App() {
  const { isLoading, selectedLanguage, isChangeBGColor } = useSelector(store => store.battle)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getBattleData(selectedLanguage))
  }, [selectedLanguage])

  return (
    <div className={`p-5 ${isChangeBGColor ? "bg-dark text-white" : ""}`}>
      <Navbar />
      <div className='d-flex justify-content-center align-items-center'>
        <LanguagesHeading />
      </div>
      <div className='ps-5 pe-5'>
        {isLoading ? (
          <div className="d-flex justify-content-center align-items-center">
            <strong>Fetching Repos</strong>
            <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
            <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
            <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
          </div>
        ) : (
          <BattleContainer />
        )}
      </div>
    </div>
  )
}

export default App