import { configureStore } from '@reduxjs/toolkit'
import BattleReducer from './Features/Battle/BattleSlice'

export const store = configureStore({
    reducer: {
        battle: BattleReducer
    }
})
