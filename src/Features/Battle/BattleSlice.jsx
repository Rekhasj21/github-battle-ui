import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

const initialState = {
    battelData: [],
    popularLanguages: ["All", "Javascipt", "Ruby", "Java", "CSS", "Python"],
    isLoading: true,
    selectedLanguage: 'All',
    isChangeBGColor: false,
    error: ''
}

export const getBattleData = createAsyncThunk('battle/getBattleData',
    async (name, thunkAPI) => {
        console.log(name)
        console.log(thunkAPI)
        try {
            const url = `https://api.github.com/search/repositories?q=stars:%3E1+language:${name}&sort=stars&order=desc&type=Repositories`
            const response = await axios(url)
            return response.data
        }
        catch (error) {
            return thunkAPI.rejectWithValue('Something went wrong')
        }
    })

const BattleSlice = createSlice({
    name: 'battle',
    initialState,
    reducers: {
        selectLanguague: (state, action) => {
            state.selectedLanguage = action.payload
        },
        changeBackgroundColor: (state) => {
            state.isChangeBGColor = !state.isChangeBGColor
        }
    },
    extraReducers: {
        [getBattleData.pending]: (state) => {
            state.isLoading = true
        },
        [getBattleData.fulfilled]: (state, action) => {
            state.isLoading = false,
                state.battelData = action.payload
        },
        [getBattleData.rejected]: (state, action) => {
            state.isLoading = false,
                state.error = action.payload
        }
    }
})

export const { selectLanguague, changeBackgroundColor } = BattleSlice.actions
export default BattleSlice.reducer