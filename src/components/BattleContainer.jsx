import { useSelector } from "react-redux"
import BattleItem from './BattleItem'

function BattleContainer() {
    const { battelData, error } = useSelector(store => store.battle)

    return (
        <div className="d-flex flex-wrap justify-content-center ps-5 pe-5">
            {!Array.isArray(battelData.items) ? (<p className="text-danger"> {error}</p>)
                : (battelData.items.map(item => {
                    return <BattleItem key={item.id} {...item} index={battelData.items.indexOf(item)} />
                }))}
        </div>
    )
}

export default BattleContainer