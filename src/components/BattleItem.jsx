import React from 'react'
import { BsFillPersonFill, BsFillExclamationTriangleFill } from 'react-icons/bs'
import { AiFillStar, AiOutlineFork } from 'react-icons/ai'
import { useSelector } from 'react-redux'

function BattleItem({ index, name, owner, forks_count, stargazers_count, open_issues, html_url }) {
    const { isChangeBGColor } = useSelector(store => store.battle)
    return (
        <div className='bg-secondary bg-opacity-25 m-2 d-flex flex-column justify-content-center align-items-start' style={{ width: '20rem', height: '30rem' }}>
            <h4 className='fs-2 fw-light align-self-center' >#{index + 1}</h4>
            <img className='align-self-center' style={{ width: '150px' }} src={owner.avatar_url} alt={name} />
            <h2 className='text-center mt-2 mb-4 align-self-center' style={{ height: '30px' }}>
                <a className='fs-4 fw-bold text-danger text-decoration-none' href={html_url} >{name}</a>
            </h2>
            <ul className='border border-0 bg-transparent'>
                <li className='d-flex justify-content-start align-items-center bg-transparent list-group-item border border-0 p-0 m-0'>
                    <div className='d-flex justify-content-start align-items-center'>
                        <BsFillPersonFill color='lightsalmon' fontSize='25px' className='m-2' />
                        <a href={`https://github.com/${owner.login}`} className={`fs-5 fw-semibold text-dark text-opacity-100 text-decoration-none 
                        ${isChangeBGColor ? 'text-white' : ''}`}>{name}</a>
                    </div>
                </li>
                <li className='d-flex justify-content-start align-items-center bg-transparent list-group-item border border-0 p-0 m-0'>
                    <div className='d-flex justify-content-start align-items-center'>
                        <AiFillStar color='gold' fontSize='25px' className='m-2' />
                        <p className={`fs-5 m-0 ${isChangeBGColor ? 'text-white' : ''}`}>{`${stargazers_count} stars`}</p>
                    </div>
                </li>
                <li className='d-flex justify-content-start align-items-center bg-transparent list-group-item border border-0 p-0 m-0'>
                    <div className='d-flex justify-content-start align-items-center'>
                        <AiOutlineFork color='lightskyblue' fontSize='25px' className='m-2' />
                        <p className={`fs-5 m-0 ${isChangeBGColor ? 'text-white' : ''}`}>{`${forks_count} forks`}</p>
                    </div>
                </li>
                <li className='d-flex justify-content-start align-items-center bg-transparent list-group-item border border-0 p-0 m-0'>
                    <div className='d-flex justify-content-start align-items-center'>
                        <BsFillExclamationTriangleFill fontSize='20px' color='lightcoral' className='m-2 ' />
                        <p className={`fs-5 m-0 ${isChangeBGColor ? 'text-white' : ''}`}>{`${open_issues} open issues`}</p>
                    </div>
                </li>
            </ul>
        </div>
    )
}

export default BattleItem