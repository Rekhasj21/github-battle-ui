import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { selectLanguague } from '../Features/Battle/BattleSlice'

function LanguagesHeading() {
    const dispatch = useDispatch()
    const { popularLanguages, selectedLanguage, isChangeBGColor } = useSelector(store => store.battle)
    return (
        <ul className='list-group list-group-horizontal' style={{width:'40rem'}} >
            {popularLanguages.map(language =>
                <li key={language} className='list-group-item fw-bold text-dark border border-0 bg-transparent'>
                    <button className={`btn btn-clear fs-5 fw-bold
                    ${(isChangeBGColor && selectedLanguage !== language) ? 'text-white' : ''}
                    ${selectedLanguage === language ? 'text-danger' : ''}  `}
                        onClick={() => dispatch(selectLanguague(language))}>{language}</button>
                </li>
            )}
        </ul>
    )
}

export default LanguagesHeading