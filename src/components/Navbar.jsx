import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { changeBackgroundColor } from '../Features/Battle/BattleSlice'

function Navbar() {
    const { isChangeBGColor } = useSelector(store => store.battle)
    const dispatch = useDispatch()
    return (
        <nav className="navbar bg-body-tertiary mt-1 ps-5 pe-5" >
            <div>
                <a className="btn text-danger fs-5 fw-bold ps-5 " href="#" role="button">Popular</a>
                <a className={`btn text-black fs-5 fw-bold " href="#" role="button" ${isChangeBGColor ? 'text-white' : ''}`}>Battle</a>
            </div>
            <div>
                {isChangeBGColor ? (
                    <button type='button' className='pe-5 btn btn-clear fs-2' onClick={() => dispatch(changeBackgroundColor())}>💡</button>
                ) : (
                    <button type='button' className='pe-5 btn btn-clear fs-2' onClick={() => dispatch(changeBackgroundColor())}>🔦</button>
                )}
            </div>
        </nav>
    )
}

export default Navbar